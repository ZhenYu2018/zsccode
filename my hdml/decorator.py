def log(level,*args, **yeal):
    def inner(func):
        def wrapper(*args, **yeal):
            print 'befor calling', func.__name__
            func(*args, **yeal)
            print "end calling", func.__name__
        return wrapper
    return inner

@log(level='high')
def hello():
    print 'hello'


@log(level='low')
def hello2(name, yeal):
    print 'my name is', name + '   my year is ', yeal


if __name__ == '__main__':
    hello()
    hello2('zhutou', 2)
