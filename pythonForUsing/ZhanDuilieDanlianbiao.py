class Stack(object):
	def __init__(self):
		self.array=[]
	def push(self,element):
		self.array.append(element)
	def pop(self):
		E=self.array[-1]
		del self.array[-1]
		return E


class Queue(object):
	def __init__(self):
		self.array=[]
	def inqueue(self,element):
		self.array.append(element)
	def outqueue(self):
		E=self.array[0]
		del self.array[0]
		return E

class Node(object):
	def __init__(self,element=0):
		self.element=element
		self.next=None

# Do a Node
n1=Node(1)
n2=Node(2)
n3=Node(3)
n4=Node(4)
n1.next=n2
n2.next=n3
n3.next=n4
